import React, { useEffect, useState } from "react";
import SimpleBarReact from "simplebar-react";
import "simplebar/src/simplebar.css";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
// import {
//   DragDropContext,
//   useDroppable,
//   useDraggable
// } from "react-beautiful-dnd";
const offset = 20;

export function App() {
  const a = React.useRef(null);
  const [h, setH] = useState(100);
  useEffect(() => {
    console.log(a.current?.getBoundingClientRect());
    setH(a.current?.getBoundingClientRect()?.top);
  }, []);
  return (
    <div>
        weq asd
      asdasdqw sdasdqwe
      <div>qweasd</div>
      <DragDropContext onDragEnd={console.log}>
        <SimpleBarReact
          forceVisible="x"
          autoHide={false}
          style={{
            overflowY: "hidden",
            overflowX: "visible",
            height: `calc(100vh - ${h}px)`,
            // maxHeight: '100%',
            width: "100%",
            position: "relative",
            display: "flex",
            flexDirection: "row",
            // gridTemplateColumns: "repeat(3, 200px)",
            gap: 20,
          }}
        >
          <div
            ref={a}
            style={{
              display: "flex",
              gap: 20,
              position: "relative",
            }}
          >
            {[...Array(20)].map((j, ind) => (
              <Droppable key={ind} droppableId={`${ind}`}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    // style={getListStyle(snapshot.isDraggingOver)}
                    {...provided.droppableProps}
                    style={{ maxHeight: "100%" }}
                  >
                    {[...Array(50)].map((x, i) => (
                      <SimpleBarReact>
                        <Draggable
                          key={`${ind}-${i}`}
                          draggableId={`${ind}-${i}`}
                          index={i}
                          style={{
                            maxHeight: `calc(100vh - ${h}px - ${offset}px)`,
                            height: "100%",
                          }}
                        >
                          {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={{
                                width: "100px",
                              }}
                            >
                              <p>{i}</p>
                            </div>
                          )}
                          {provided.placeholder}
                        </Draggable>
                      </SimpleBarReact>
                    ))}
                  </div>
                )}
              </Droppable>
            ))}
          </div>
        </SimpleBarReact>
      </DragDropContext>
      {/* </div> */}
    </div>
  );
}


FROM nginx:stable-alpine

COPY build /usr/share/nginx/html

EXPOSE 80

WORKDIR /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]